package lounastin.fi.lounastin.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.R;
import lounastin.fi.lounastin.models.Restaurant;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {
    private List<Restaurant> restaurants;

    public RestaurantListAdapter(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list_item, parent, false);
        return new RestaurantViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        holder.nameView.setText(
                restaurants.get(position).getName()
        );

        // Save restaurant ID so it can be fetched later
        holder.selectBtn.setTag(restaurants.get(position).getId());
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }
}

class RestaurantViewHolder extends RecyclerView.ViewHolder {
    TextView nameView;
    Button selectBtn;

    RestaurantViewHolder(@NonNull LinearLayout itemView) {
        super(itemView);
        nameView = itemView.findViewById(R.id.restaurant_name);
        selectBtn = itemView.findViewById(R.id.restaurant_select);
    }
}
