package lounastin.fi.lounastin.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.R;
import lounastin.fi.lounastin.models.Review;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    List<Review> reviews;

    public ReviewListAdapter(List<Review> reviews) {
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false);
        return new ReviewViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewViewHolder holder, int position) {
        Review review = reviews.get(position);
        holder.nameView.setText(review.getReviewer().getFullName());
        holder.commentView.setText(review.getComment());
        holder.dateView.setText(review.getReviewDateString());
        holder.ratingView.setRating(review.getRating());
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }
}

class ReviewViewHolder extends RecyclerView.ViewHolder {
    TextView nameView;
    TextView commentView;
    TextView dateView;
    RatingBar ratingView;


    public ReviewViewHolder(@NonNull ConstraintLayout itemView) {
        super(itemView);
        nameView = itemView.findViewById(R.id.user_name);
        commentView = itemView.findViewById(R.id.user_comment);
        dateView = itemView.findViewById(R.id.review_date);
        ratingView = itemView.findViewById(R.id.ratingBar);
    }
}
