package lounastin.fi.lounastin.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.R;
import lounastin.fi.lounastin.ViewReviewActivity;
import lounastin.fi.lounastin.models.Food;

/**
 * Adapter for displaying menus
 */
public class FoodListAdapter extends RecyclerView.Adapter<FoodViewHolder> {
    List<Food> foods;

    public FoodListAdapter(List<Food> foods) {
        this.foods = foods;
    }

    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_item, parent, false);
        return new FoodViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        Food food = foods.get(position);

        // Set values
        holder.titleView.setText(food.getTitle());
        holder.descriptionView.setText(food.getDescription());
        holder.dietView.setText(food.getDiet());

        holder.titleView.setTag(food.getId());
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }
}

class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView titleView;
    TextView descriptionView;
    TextView dietView;

    public FoodViewHolder(@NonNull LinearLayout itemView) {
        super(itemView);

        titleView = itemView.findViewById(R.id.food_title);
        descriptionView = itemView.findViewById(R.id.food_description);
        dietView = itemView.findViewById(R.id.food_diet);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent reviewIntent = new Intent(v.getContext(), ViewReviewActivity.class);
        reviewIntent.putExtra("FOOD_ID", (int) titleView.getTag());
        v.getContext().startActivity(reviewIntent);
    }
}
