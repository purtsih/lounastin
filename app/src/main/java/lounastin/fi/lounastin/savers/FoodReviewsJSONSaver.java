package lounastin.fi.lounastin.savers;

import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import lounastin.fi.lounastin.models.Food;
import lounastin.fi.lounastin.models.Review;

/**
 * Class for saving foods and their reviews to disk in JSON format
 */
public class FoodReviewsJSONSaver {
    private String convertFoodListToJson(List<Food> foods) {
        /*
         * Schema:
         * {
         *      "foods": [
         *          {
         *              "id": 1
         *              "name": "Makaronilaatikko",
         *              "description": "Sisältää jauhelihaa ja makaroonia",
         *              "diet": "L, G",
         *              "reviews": [
         *                  {
         *                      "reviewer": "Jaska Jokunen",
         *                      "rating": 3,
         *                      "comment": "Hyvää oli"
         *                  }
         *              ]
         *          },
         *          ...
         *      ]
         * }
         */
        JSONObject base = new JSONObject(); // Base object for output
        try {
            JSONArray foodArray = new JSONArray(); // An array that holds our food objects
            for (Food food : foods) {
                JSONObject foodPiece = new JSONObject(); // A single food object
                foodPiece.put("id", food.getId());
                foodPiece.put("title", food.getTitle());
                foodPiece.put("description", food.getDescription());
                foodPiece.put("diet", food.getDiet());

                // Process reviews
                List<Review> reviews = food.getReviews();
                JSONArray reviewArray = new JSONArray();
                for (Review review : reviews) {
                    JSONObject reviewPiece = new JSONObject();
                    reviewPiece.put("reviewer", review.getReviewer().getFullName());
                    reviewPiece.put("rating", review.getRating());
                    reviewPiece.put("comment", review.getComment());
                    reviewPiece.put("reviewed_at", dateToISO8601(review.getReviewDate()));

                    reviewArray.put(reviewPiece);
                }
                foodPiece.put("reviews", reviewArray);
                foodArray.put(foodPiece);
            }
            base.put("foods", foodArray);
        } catch (JSONException exp) {
            exp.printStackTrace();
        }

        return base.toString();
    }

    /**
     * Saves food reviews to disk
     * @param filename Filename
     * @param foods List of foods to be saved.
     */
    public void save(String filename, List<Food> foods) {
        String foodsJson = convertFoodListToJson(foods);
        Log.i("FoodReviewJSONSaver", foodsJson);

        if (! isExternalStorageWriteable()) {
            Log.e("FoodReviewSaver", "Storage not writeable");
            return;
        }

        // Get public directory and open a file to save reviews to
        File directory = getTextStorageDirectory("Lounastin");
        File savefile = new File(directory, "food_reviews.json");
        Log.i("FoodReviewSaver", savefile.toString()); // Debug path

        try {
            OutputStreamWriter writer = new FileWriter(savefile);
            writer.write(foodsJson);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Converts Date object into ISO 8601 date format string.
     * JSON doesn't define a date standard, but Javascript's JSON.stringify() produces ISO 8601 strings for dates
     * and they are our best bets.
     * @param date Date
     * @return ISO 8061 formatted string.
     */
    @NonNull
    private String dateToISO8601(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'", Locale.ENGLISH);
        df.setTimeZone(tz);

        return df.format(date);
    }

    /*
        Sdcard helpers
     */
    private boolean isExternalStorageWriteable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private File getTextStorageDirectory(String name) {
        // Open a directory in public documents folder
        File file = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                name
        );
        if (! file.mkdirs()) {
            Log.e("FoodReviewsSaver", "Directory not created");
        }
        return file;
    }
}
