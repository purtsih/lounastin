package lounastin.fi.lounastin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.adapters.ReviewListAdapter;
import lounastin.fi.lounastin.models.Food;
import lounastin.fi.lounastin.models.Review;
import lounastin.fi.lounastin.stores.FoodStore;
import lounastin.fi.lounastin.stores.UserStore;

public class ViewReviewActivity extends AppCompatActivity {
    RecyclerView reviewList;
    ReviewListAdapter adapter;
    Button newReviewButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_review);
        newReviewButton = findViewById(R.id.new_review_button);

        Food food = FoodStore.getInstance().findById(getFoodIdFromBundle(savedInstanceState));
        List<Review> reviews;
        if (food != null) {
            reviews = food.getReviews();
        } else {
            reviews = new ArrayList<>();
        }
        newReviewButton.setTag(food.getId());

        if (UserStore.getInstance().getCurrentUser() == null) {
            newReviewButton.setEnabled(false); // Disable reviews until user logs in
        }

        reviewList = findViewById(R.id.review_list);
        adapter = new ReviewListAdapter(reviews);
        reviewList.setLayoutManager(new LinearLayoutManager(this));
        reviewList.setAdapter(adapter);
        reviewList.setHasFixedSize(true);
    }

    public void navigateToNewReview(View view) {
        int id = (int)view.getTag();
        Intent reviewIntent = new Intent(this, NewReviewActivity.class);
        reviewIntent.putExtra("FOOD_ID", id);
        startActivity(reviewIntent);
    }

    private int getFoodIdFromBundle(Bundle savedInstanceState) {
        int id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = 1; // Some default, usually exists
            } else {
                id = extras.getInt("FOOD_ID");
            }
        } else {
            id = (int) savedInstanceState.getSerializable("FOOD_ID");
        }
        return id;
    }
}
