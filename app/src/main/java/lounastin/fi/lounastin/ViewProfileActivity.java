package lounastin.fi.lounastin;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import lounastin.fi.lounastin.models.User;
import lounastin.fi.lounastin.stores.UserStore;
import lounastin.fi.lounastin.util.Hash;

public class ViewProfileActivity extends AppCompatActivity {
    private User user;

    private EditText firstnameBox;
    private EditText lastnameBox;
    private EditText passwordBox;
    private EditText emailBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        user = getUserFromBundle(savedInstanceState);

        firstnameBox = findViewById(R.id.edit_firstname);
        lastnameBox = findViewById(R.id.edit_lastname);
        passwordBox = findViewById(R.id.edit_password);
        emailBox = findViewById(R.id.edit_email);

        bindUserDetailsToView(user);
    }

    private void bindUserDetailsToView(User user) {
        firstnameBox.setText(user.getFirstName());
        lastnameBox.setText(user.getLastname());
        emailBox.setText(user.getUsername());
    }

    @Nullable User getUserFromBundle(Bundle bundle) {
        int id;
        String USER_KEY = "USER_ID";
        if (bundle == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = 0; // Some default, usually exists
            } else {
                id = extras.getInt(USER_KEY);
            }
        } else {
            id = (int) bundle.getSerializable(USER_KEY);
        }

        UserStore store = UserStore.getInstance();
        return store.findById(id);
    }

    public void saveProfile(View view) {
        // @todo allow changing email (needs to consult UserStore to avoid username collisions)
        user.setFirstname(firstnameBox.getText().toString().trim().isEmpty() ? user.getFirstName() : firstnameBox.getText().toString());
        user.setLastname(lastnameBox.getText().toString().trim().isEmpty() ? user.getLastname() : lastnameBox.getText().toString());

        if (! passwordBox.getText().toString().trim().isEmpty()) {
            // User wants to set a new password
            user.setPasswordHash(Hash.make(passwordBox.getText().toString()));
        }

        Toast.makeText(this, "Profiili tallennettu!", Toast.LENGTH_LONG)
                .show();
        finish();
    }
}
