package lounastin.fi.lounastin;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import lounastin.fi.lounastin.models.Food;
import lounastin.fi.lounastin.models.Review;
import lounastin.fi.lounastin.models.User;
import lounastin.fi.lounastin.stores.FoodStore;
import lounastin.fi.lounastin.stores.UserStore;

/**
 * Activity for making a new review
 */
public class NewReviewActivity extends AppCompatActivity {
    Food food;
    User reviewer;

    EditText commentView;
    RatingBar ratingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_review);

        // Get views
        commentView = findViewById(R.id.food_comment);
        ratingView = findViewById(R.id.review_rating);

        // Get entities
        food = FoodStore.getInstance()
                .findById(getFoodIdFromBundle(savedInstanceState));
        reviewer = UserStore.getInstance().getCurrentUser();
        if (food == null || reviewer == null)
            finish(); // Quit activity if food doesn't exist

        getSupportActionBar().setTitle(
                getString(R.string.give_review_title, food.getTitle())
        );
    }

    private int getFoodIdFromBundle(Bundle savedInstanceState) {
        int id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = 1; // Some default, usually exists
            } else {
                id = extras.getInt("FOOD_ID");
            }
        } else {
            id = (int) savedInstanceState.getSerializable("FOOD_ID");
        }
        return id;
    }

    public void submitReview(View view) {
        Review review = new Review((int)ratingView.getRating(), commentView.getText().toString(), new Date(), reviewer);
        food.addReview(review);

        Toast.makeText(this, "Kiitos arvostelustasi!", Toast.LENGTH_SHORT).show();
        
        finish();
    }
}
