package lounastin.fi.lounastin;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.adapters.FoodListAdapter;
import lounastin.fi.lounastin.models.Food;
import lounastin.fi.lounastin.models.Menu;
import lounastin.fi.lounastin.models.Restaurant;
import lounastin.fi.lounastin.stores.RestaurantStore;
import lounastin.fi.lounastin.util.DayOfWeek;

public class RestaurantActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Restaurant restaurant;

    RecyclerView foodList;
    Spinner daySelectSpinner;
    TextView noLunchTodayText;
    FoodListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        // Set references
        foodList = findViewById(R.id.food_list);
        daySelectSpinner = findViewById(R.id.day_select);
        noLunchTodayText = findViewById(R.id.no_lunch_message);

        daySelectSpinner.setOnItemSelectedListener(this);

        // Find selected restaurant
        int restaurantId = getRestaurantIdFromBundle(savedInstanceState);
        restaurant = RestaurantStore.getInstance().findById(restaurantId);

        // Set restaurant name to the action bar
        getSupportActionBar()
                .setTitle(restaurant.getName());

        // Prepare adapter
        adapter = new FoodListAdapter(getCurrentlySelectedFoods());
        foodList.setAdapter(adapter);
        foodList.setLayoutManager(new LinearLayoutManager(this));
        foodList.setHasFixedSize(true);
    }

    List<Food> getCurrentlySelectedFoods() {
        // Get menu according to user day selection
        Menu menu = restaurant.getMenuForDay(
                daySelectSpinnerToWeekday(daySelectSpinner.getSelectedItemPosition())
        );
        List<Food> foods;
        if (menu != null) {
            foods = menu.getFoods();
        } else {
            foods = new ArrayList<>(); // Produces empty list in the end
        }
        return foods;
    }

    private int getRestaurantIdFromBundle(Bundle savedInstanceState) {
        int id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = 1; // Some default, usually exists
            } else {
                id = extras.getInt("RESTAURANT_ID");
            }
        } else {
            id = (int) savedInstanceState.getSerializable("RESTAURANT_ID");
        }
        return id;
    }

    /**
     * Converts spinner (dropdown) position to DayOfWeek enum
     * @param position Spinner position
     * @return Day of week
     */
    DayOfWeek daySelectSpinnerToWeekday(int position) {
        switch (position) {
            case 0:
                return DayOfWeek.MONDAY;
            case 1:
                return DayOfWeek.TUESDAY;
            case 2:
                return DayOfWeek.WEDNESDAY;
            case 3:
                return DayOfWeek.THURSDAY;
            case 4:
                return DayOfWeek.FRIDAY;
            default: // Also includes handles Spinner.INVALID_VALUE
                return DayOfWeek.MONDAY;
        }
    }

    /**
     * Sets visibility of "No lunch today" message
     * @param yes Whether there is food today
     */
    public void setHasFoods(boolean yes) {
        noLunchTodayText.setVisibility(yes ? View.INVISIBLE : View.VISIBLE);
    }

    /**
     * Fetches current spinner state and updates food list accordingly
     */
    public void updateFoods() {
        List<Food> newFoods = getCurrentlySelectedFoods();
        adapter.setFoods(newFoods);
        setHasFoods(newFoods.size() > 0);
        adapter.notifyDataSetChanged();
    }

    /**
     * Fired when a day is selected
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        updateFoods();
    }

    /**
     * Fired when no day is selected
     * @param parent
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        updateFoods();
    }
}
