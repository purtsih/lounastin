package lounastin.fi.lounastin.stores;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import lounastin.fi.lounastin.models.Food;

public class FoodStore {
    List<Food> foods;
    private static volatile FoodStore instance;

    private FoodStore() {
        foods = new ArrayList<>();
    }

    @NonNull
    public static synchronized FoodStore getInstance() {
        if (instance == null) {
            instance = new FoodStore();
        }

        return instance;
    }

    public void insert(Food food) {
        foods.add(food);
    }
    public void insertAll(Food... foods) {
        for (Food food : foods) {
            insert(food);
        }
    }

    public Food findById(int id) {
        for (Food food : foods) {
            if (food.getId() == id) {
                return food;
            }
        }
        return null;
    }

    public List<Food> findAll() {
        return foods;
    }
}
