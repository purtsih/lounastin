package lounastin.fi.lounastin.stores;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import lounastin.fi.lounastin.models.Restaurant;

/**
 * Keeps track of all restaurants
 */
public class RestaurantStore {
    List<Restaurant> restaurants;
    private static volatile RestaurantStore instance;

    private RestaurantStore() {
        restaurants = new ArrayList<>();
    }

    @NonNull
    public static synchronized RestaurantStore getInstance() {
        if (instance == null) {
            instance = new RestaurantStore();
        }

        return instance;
    }

    public void insert(Restaurant rest) {
        restaurants.add(rest);
    }

    public Restaurant findById(int id) {
        for (Restaurant rest : restaurants) {
            if (rest.getId() == id) {
                return rest;
            }
        }
        return null;
    }

    public List<Restaurant> findAll() {
        return restaurants;
    }
}
