package lounastin.fi.lounastin.stores;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import lounastin.fi.lounastin.models.User;

/**
 * Global user store.
 * Stores all user entities and allows simple searching.
 *
 * This, as all other Stores are
 */
public class UserStore {
    private List<User> users;
    private User currentUser;
    private static volatile UserStore instance;

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    // Private constructor to prevent instantiation outside of getInstance()
    private UserStore() {
        users = new ArrayList<>();
    }

    /**
     * Get an UserStore instance.
     * synchronized and volatile keywords makes this
     * singleton work in multi-threaded environments.
     * @return UserStore instance
     */
    @NonNull
    public static synchronized UserStore getInstance() {
        if (instance == null) {
            instance = new UserStore();
        }

        return instance;
    }

    @Nullable
    public User findOneByUsername(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }

        return null;
    }

    /**
     * Save a new user
     * @param user User to be saved
     * @return True if user was successfully saved, false otherwise
     */
    public boolean insert(User user) {
        // Simple, but inefficient unique checking
        if (findOneByUsername(user.getUsername()) != null) {
            return false;
        }
        users.add(user);
        return true;
    }

    public @Nullable User findById(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }

        return null;
    }
}
