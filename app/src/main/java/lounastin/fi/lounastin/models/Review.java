package lounastin.fi.lounastin.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A single review.
 * Review contains reference to what food was reviewed, a score and a free-form review text
 */
public class Review {
    private int score;
    private String comment;
    private Date created_at;
    private User reviewer;

    public Review(int score, String comment, Date created_at, User reviewer) {
        this.score = score;
        this.comment = comment;
        this.created_at = created_at;
        this.reviewer = reviewer;
    }

    public User getReviewer() {
        return reviewer;
    }

    public String getComment() {
        return comment;
    }

    public int getRating() {
        return score;
    }

    public String getReviewDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.ENGLISH);
        return format.format(created_at);
    }

    public Date getReviewDate() {
        return created_at;
    }
}
