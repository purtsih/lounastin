package lounastin.fi.lounastin.models;

import java.util.HashMap;

import androidx.annotation.Nullable;
import lounastin.fi.lounastin.util.DayOfWeek;

/**
 * Represents a restaurant.
 *
 * Restaurants contain a list of foods, a menu
 */
public class Restaurant {
    int id;
    String name;
    HashMap<DayOfWeek, Menu> menus;

    public Restaurant(int id, String name) {
        this.id = id;
        this.name = name;

        menus = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    @Nullable
    public Menu getMenuForDay(DayOfWeek day) {
        return menus.get(day);
    }

    public void setMenuForDay(DayOfWeek day, Menu menu) {
        menus.put(day, menu);
    }

    public int getId() {
        return id;
    }
}
