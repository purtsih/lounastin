package lounastin.fi.lounastin.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a food item.
 * Usually part of a menu
 */
public class Food {
    public int id;
    public String title;
    public String description;
    private String diet;
    private List<Review> reviews;

    public Food(int id, String title, String description, String diet) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.diet = diet;
        this.reviews = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDiet() {
        return diet;
    }

    public void addReview(Review review) {
        reviews.add(review);
    }

    public List<Review> getReviews() {
        return reviews;
    }

    @Override
    public String toString() {
        return getTitle() + ": " + getDescription() + " (" + getDiet() + ")";
    }

    public int getId() {
        return id;
    }
}
