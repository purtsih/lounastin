package lounastin.fi.lounastin.models;

import java.util.List;

/**
 * Represents a menu for a single day.
 * Simply contains a list of foods served that day
 */
public class Menu {
    private List<Food> foods;

    public Menu(List<Food> foods) {
        this.foods = foods;
    }

    public List<Food> getFoods() {
        return foods;
    }
}
