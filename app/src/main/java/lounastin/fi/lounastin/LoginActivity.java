package lounastin.fi.lounastin;

import androidx.appcompat.app.AppCompatActivity;
import lounastin.fi.lounastin.models.User;
import lounastin.fi.lounastin.stores.UserStore;
import lounastin.fi.lounastin.util.Hash;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    EditText usernameText;
    EditText passwordText;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameText = findViewById(R.id.username_input);
        passwordText = findViewById(R.id.password_input);
        message = findViewById(R.id.login_message);
    }

    /**
     * Login button click handler
     * @param view View
     */
    public void login(View view) {
        // Again, this really be a network request or something
        User user = UserStore.getInstance().findOneByUsername(usernameText.getText().toString());
        if (user != null) {
            // User exists, compare passwords
            if (Hash.verify(passwordText.getText().toString(), user.getPasswordHash())) {
                // Password correct
                message.setText(R.string.login_success);
                UserStore.getInstance().setCurrentUser(user);

                // Closes this activity and returns to the main
                finish();
            } else {
                message.setText(R.string.login_wrong_password);
            }
        } else {
            // User not found
            message.setText(R.string.login_wrong_user);
        }
    }
}

