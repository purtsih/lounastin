package lounastin.fi.lounastin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import lounastin.fi.lounastin.models.Food;
import lounastin.fi.lounastin.models.Menu;
import lounastin.fi.lounastin.models.Restaurant;
import lounastin.fi.lounastin.models.User;
import lounastin.fi.lounastin.savers.FoodReviewsJSONSaver;
import lounastin.fi.lounastin.stores.FoodStore;
import lounastin.fi.lounastin.stores.RestaurantStore;
import lounastin.fi.lounastin.stores.UserStore;
import lounastin.fi.lounastin.util.DayOfWeek;
import lounastin.fi.lounastin.util.Hash;

public class MainActivity extends AppCompatActivity {
    TextView welcomeText;

    // Button references for controlling visibility
    Button logoutBtn;
    Button myprofileBtn;
    Button loginBtn;

    static boolean dummyDataSeeded = false;

    /**
     * Makes some dummy data for the application.
     * In real world this would come from the server or something.
     */
    void makeDummyData() {
        if (dummyDataSeeded)
            return;

        UserStore userStore = UserStore.getInstance();
        User brian = new User(
                1, "Brian", "Kottarainen", "brian@example.tld", Hash.make("password")
        );
        User jaska = new User(
                2, "Jaska", "Jokunen", "jaska@example.tld", Hash.make("password")
        );

        userStore.insert(brian);
        userStore.insert(jaska);

        // Create some restaurants
        Restaurant yolo = new Restaurant(1, "Ylioppilastalo");
        Restaurant buffa = new Restaurant(2, "LUT Buffer");
        Restaurant laser = new Restaurant(3, "Laser");

        // Some foods
        Food maklaat = new Food(1,"Makaronilaatikko", "Makaroonia ja jauhelihaa", "L");
        Food salvbloir = new Food(2,"Slaavilaista broileria", "Idästä", "L, G");
        Food hernari = new Food(3,"Hernekeitto", "", "L, G, M, Kela");
        Food kebakot = new Food(4,"Pötkylät", "Klassikko", "L, G");
        Food riisipuuro = new Food(5, "Riisipuuro", "", "L, G, M");
        Food pasta = new Food(6, "Pasta Bolognese", "Herkullinen pasta", "M, G");
        Food salaatti = new Food(7, "Talon salaatti", "Salaattia talon tapaan", "L, G");

        // Register available foods
        FoodStore fs = FoodStore.getInstance();
        fs.insertAll(maklaat, salvbloir, hernari, kebakot, riisipuuro, pasta, salaatti);

        // Put foods into menus
        Menu menu1 = new Menu(Arrays.asList(maklaat, salvbloir));
        Menu menu2 = new Menu(Arrays.asList(maklaat, hernari, kebakot));

        // Bind menus to restaurants
        buffa.setMenuForDay(DayOfWeek.MONDAY, new Menu(Arrays.asList(maklaat, salvbloir)));
        buffa.setMenuForDay(DayOfWeek.TUESDAY, new Menu(Arrays.asList(salvbloir)));
        buffa.setMenuForDay(DayOfWeek.WEDNESDAY, new Menu(Arrays.asList(riisipuuro, hernari)));
        buffa.setMenuForDay(DayOfWeek.THURSDAY, new Menu(Arrays.asList(hernari)));
        buffa.setMenuForDay(DayOfWeek.FRIDAY, new Menu(Arrays.asList(salaatti, hernari)));


        yolo.setMenuForDay(DayOfWeek.MONDAY, menu1);
        yolo.setMenuForDay(DayOfWeek.TUESDAY, menu2);
        yolo.setMenuForDay(DayOfWeek.WEDNESDAY, new Menu(Arrays.asList(riisipuuro, salaatti)));
        yolo.setMenuForDay(DayOfWeek.THURSDAY, new Menu(Arrays.asList(pasta)));
        yolo.setMenuForDay(DayOfWeek.FRIDAY, new Menu(Arrays.asList(hernari, pasta)));

        laser.setMenuForDay(DayOfWeek.MONDAY, menu1);
        laser.setMenuForDay(DayOfWeek.TUESDAY, menu1);
        laser.setMenuForDay(DayOfWeek.WEDNESDAY, menu2);
        laser.setMenuForDay(DayOfWeek.THURSDAY, menu1);
        laser.setMenuForDay(DayOfWeek.FRIDAY, menu1);


        // Register available restaurants
        RestaurantStore rest = RestaurantStore.getInstance();
        rest.insert(buffa);
        rest.insert(yolo);
        rest.insert(laser);

        dummyDataSeeded = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        welcomeText = findViewById(R.id.main_header);

        loginBtn = findViewById(R.id.login_btn);
        logoutBtn = findViewById(R.id.logout_btn);
        myprofileBtn = findViewById(R.id.my_profile_btn);

        makeDummyData();
    }

    /**
     * Called when activity returns to the foreground
     */
    @Override
    protected void onResume() {
        super.onResume();

        // Update welcome text
        User currentUser = UserStore.getInstance().getCurrentUser();
        handleLoginState(currentUser);
    }

    public void navigateToLogin(View view) {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    public void navigateToLunches(View view) {
        Intent lunchIntent = new Intent(this, RestaurantListActivity.class);
        startActivity(lunchIntent);
    }

    public void logout(View view) {
        UserStore store = UserStore.getInstance();

        // Simply set current user to null
        if (store.getCurrentUser() != null) {
            store.setCurrentUser(null);
            refreshActivity();
        }
    }

    public void navigateToMyProfile(View view) {
        User user = UserStore.getInstance().getCurrentUser();

        if (user != null) {
            Intent intent = new Intent(this, ViewProfileActivity.class);
            intent.putExtra("USER_ID", user.getId());
            startActivity(intent);
        }
    }

    /**
     * Refreshes (stops and starts) MainActivity
     */
    public void refreshActivity() {
        finish();
        overridePendingTransition(0, 0); // Disables transitions so this is instant
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    public void saveReviewsAsJSON(View view) {
        final String filename = "food_reviews.json";

        // Test JSON saving
        FoodReviewsJSONSaver saver = new FoodReviewsJSONSaver();
        saver.save(filename, FoodStore.getInstance().findAll());

        Toast.makeText(this, getString(R.string.saved_reviews_to_file, filename), Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * Sets visibility for elements depending if user has logged in
     */
    public void handleLoginState(@Nullable User user) {
        if (user != null) {
            welcomeText.setText(
                    getString(R.string.welcome_text_user, user.getFirstName())
            );
            loginBtn.setVisibility(View.INVISIBLE);
            logoutBtn.setVisibility(View.VISIBLE);
            myprofileBtn.setVisibility(View.VISIBLE);
        } else {
            loginBtn.setVisibility(View.VISIBLE);
            logoutBtn.setVisibility(View.INVISIBLE);
            myprofileBtn.setVisibility(View.INVISIBLE);
            welcomeText.setText(R.string.welcome_text_guest);
        }
    }
}
