package lounastin.fi.lounastin.util;

/**
 * Our version of java.time.DayOfWeek
 * Original enum requires higher Android API level (26).
 * We are currently locked into level 24 by requirements.
 */
public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY
}
