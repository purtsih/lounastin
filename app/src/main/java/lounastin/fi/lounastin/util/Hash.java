package lounastin.fi.lounastin.util;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Simple class for making password hashes.
 * This uses very weak SHA-512 hashing with constant salt, which is not secure at all.
 */
public class Hash {
    private static final String SALT = "1234567890";

    private static byte[] digest(String plaintext) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA512"); // Weak algo

            sha.update((plaintext + SALT).getBytes(Charset.forName("UTF-8")));

            return sha.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String bytesToString(byte[] bytes) {
        return new BigInteger(1, bytes).toString();
    }

    public static String make(String plaintext) {
        byte[] hash = digest(plaintext);
        return bytesToString(hash);
    }

    public static boolean verify(String plaintext, String hash) {
        return make(plaintext).equals(hash); // This is not constant time, insecure
    }
}
