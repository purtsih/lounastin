package lounastin.fi.lounastin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lounastin.fi.lounastin.adapters.RestaurantListAdapter;
import lounastin.fi.lounastin.stores.RestaurantStore;

/**
 * Activity for browsing lunch lists
 */
public class RestaurantListActivity extends AppCompatActivity {
    RecyclerView restaurantListView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);

        // Get references and construct recyclerview related objects
        restaurantListView = findViewById(R.id.restaurants_list);
        adapter = new RestaurantListAdapter(RestaurantStore.getInstance().findAll());
        layoutManager = new LinearLayoutManager(this);

        // Set up recyclerview
        restaurantListView.setAdapter(adapter);
        restaurantListView.setLayoutManager(layoutManager);
        restaurantListView.setHasFixedSize(true);
    }

    /**
     * User pressed the button next to restaurant
     * @param view
     */
    public void onRestaurantSelected(View view) {
        // Restaurant ID is stored in the button's tag
        int id = (int)view.getTag();

        Intent restIntent = new Intent(this, RestaurantActivity.class);
        restIntent.putExtra("RESTAURANT_ID", id);
        startActivity(restIntent);
    }
}
